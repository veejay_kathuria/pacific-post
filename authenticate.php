<?php

require "pdo.php";

session_start();



if (isset($_POST['signin'])){

    if(isset($_POST['username']) && isset($_POST['password']) && strlen($_POST['username'])>0 && strlen($_POST['password'])>0){

        $check = base64_encode($_POST['password']);

        // $check = $_POST['password'];

        $stmt = $pdo->prepare('SELECT user_id, password FROM user

            WHERE username = :us');

        $stmt->execute(array( ':us' => $_POST['username']));

        $row = $stmt->fetch(PDO::FETCH_ASSOC);


        $stm= $pdo->prepare("select * from maintenance");
                $stm->execute();
                $maintain= $stm->Fetch(PDO::FETCH_ASSOC);


                if( $maintain["is_active"]){

                    $st = $pdo->prepare ("SELECT p.permission_name FROM user as u INNER JOIN role_2_permission as r2p ON u.role_id = r2p.role_id inner JOIN permission as p ON r2p.permission_id= p.permission_id WHERE u.user_id= :uid and p.permission_name='Edit Maintenance Mode' ;");
                    $st->execute (array(        ':uid' => $row['user_id']) );
                    $permission= $st->Fetch(PDO::FETCH_ASSOC);

                    if(empty($permission)){
                        header("Location: index.php");
                        return;
                    }

                }


        if($check == $row['password']){

            $_SESSION["user_id"]= $row['user_id'];

            $_SESSION["username"] =$_POST['username'];

            $_SESSION["success"] = "login success";





            $stmt = $pdo->prepare ("SELECT user_id , client_id FROM user_2_client where user_id= :uid");

            $stmt->execute (array(        ':uid' => $_SESSION["user_id"]));

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

                if(isset($row['user_id'])){

                    $_SESSION['client_id'] = $row['client_id'];

                    header("Location: client.php?client_id=".urldecode($row['client_id']));

                    return;

                }

            }


            header("Location: dashboard.php");

            return;

        }

        else{

            $_SESSION["error"] = "There was an error logging you in. Please verify your username and password and try again.";

            header("Location: index.php");

            return;



        }

    } else{

        $_SESSION["error"] = "There was an error logging you in. Please verify your username and password and try again.";

        header("Location: index.php");

        return;

    }

}
