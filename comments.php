<?php
// require "pdo.php";
// session_start();

require "header.php";


if(!isset($_SESSION['client_id'])){
require "sidebar.php";
}
$_SESSION['present']= $_SERVER[REQUEST_URI];


function getUsername($userId){
    require "pdo.php";
    $stmt = $pdo->prepare ("SELECT username from user where user_id= :uid;");
    $stmt->execute (array(':uid'=> $userId ));
    $usersname = $stmt->fetch(PDO::FETCH_ASSOC);

    return $usersname['username'];
}






if(!isset($_SESSION['client_id'])){
?>
        <!-- PAGE CONTAINER-->
        <div class="page-container">
<?php } else { ?>

        <div class= "container">

<?php }   ?>


<?php
$stmt = $pdo->prepare ("SELECT * from uploads as u LEFT JOIN user as us on u.user_id= us.user_id left join upload_2_client as u2c on u2c.upload_id=u.upload_id left join clients as c on u2c.client_id = c.client_id where u.upload_id = :upid and u2c.client_id = :cid;");
$stmt->execute (array(':upid'=> $_GET['upload_id'], 'cid'=> $_GET['client_id']));

$row = $stmt->fetch(PDO::FETCH_ASSOC);



?>
            <!-- MAIN CONTENT-->
            <div class="main-content container">
<?php
if( isset($_SESSION["error"])){   echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';  unset($_SESSION["error"]); }
if( isset($_SESSION["success"])){   echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';  unset($_SESSION["success"]); }


?>
                <section class="media-view">
                    <div class="col-12 bg-light p-4" id="media-file-wrap">
                        <div class="row">
                        <div class="col-sm-6 p-2">
                            <div align="center" class="embed-responsive embed-responsive-16by9">
                            <?php
                            $getFileName=explode('.',$row['filename']);
                            if(in_array($getFileName[1],["jpeg","jpg","png","gif"])){
                                if($row['is_client_media'] === NULL){     ?>
                                    <img class="embed-responsive-item" src="/uploads/<?=$row['filename']?>">

                            <?php } else { ?>
                                <img class="embed-responsive-item" src="uploads/client_uploads/<?=$row['filename']?>">

                            <?php }
                            }  else if(in_array($getFileName[1],["mp4"])){
                                if($row['is_client_media'] === NULL){     ?>
                                    <video controls loop class="embed-responsive-item">
                                    <source src="/uploads/<?=$row['filename']?>" type="video/mp4">

                            <?php } else { ?>
                                <video controls loop class="embed-responsive-item">
                                    <source src="/uploads/client_uploads/<?=$row['filename']?>" type="video/mp4">

                            <?php }
                            } else if(in_array($getFileName[1],["txt","php","xml","js","html","docx"])){ ?>
                                <img class="embed-responsive-item" src="/uploads/txtfile-placeholder.png">

                            <?php } else if(in_array($getFileName[1],["pdf"])){ ?>
                                <img class="embed-responsive-item" src="/uploads/pdf-placeholder.png">

                            <?php } else if(in_array($getFileName[1],["zip"])){ ?>
                                <img class="embed-responsive-item" src="/uploads/zip-placeholder.png">

                            <?php } else if(in_array($getFileName[1],["ppt"])){ ?>
                                <img class="embed-responsive-item" src="/uploads/ppt-placeholder.png">

                            <?php } else if(in_array($getFileName[1],["xls","csv"])){ ?>
                                <img class="embed-responsive-item" src="/uploads/xls-placeholder.png">

                            <?php } else if(in_array($getFileName[1],["exe","dmg"])){ ?>
                                <img class="embed-responsive-item" src="/uploads/setup-placeholder.png">

                            <?php } else{ ?>
                                <img class="embed-responsive-item" src="/uploads/ms-placeholder.png">

                            <?php } ?>
                                <!--  <video controls loop class="embed-responsive-item">
                                    <source src="https://www.pacificpostedit.com/common/videos/%20Little_Passports-Interview%20final_3-H.264_LAN_TEST2.mp4" type="video/mp4"> -->
                            </div>
                        </div>


                        <div class="col-sm-6 p-2">
                        <div class="top-campaign">
                                    <h3 class="h4 mb-3"><?=$row['client_name']?></h3>
                                    <div class="table-responsive">
                                        <table class="table table-top-campaign">
                                            <tbody id="mediaDetailTbody">
                                                <tr>
                                                    <td class="text-dark">Uploaded by</td>
                                                    <td class="text-muted"><?=$row['username']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-dark">Filename</td>
                                                    <td class="text-muted"><?=$row['filename']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-dark">Size</td>
                                                    <td class="text-muted"><code><?=formatSizeUnits($row['upload_size'])?></code></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-dark">Date Added</td>
                                                    <td class="text-muted"><?=$row['upload_date']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-dark">Description</td>
                                                    <td class="text-muted"><?=$row['upload_subtitle']?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                        </div>

                        <section id="comments-section">
                            <div class="row">
                                <div class="col-sm-12 mb-4 mt-4">
                                    <h6 class="title mb-3">
                                        Add Comment
                                    </h6>
                                    <form action="add.php?case=5&upload_id=<?=urlencode($_GET['upload_id'])?>&client_id=<?=$_GET['client_id']?>" method="post" class="comments-form">
                                    <div class="input-group">
                                        <textarea name="commentText" class="form-control" aria-label="textarea" style="resize:none" placeholder="Write comment here..."></textarea>
                                    </div>
                                    <p class="float-left p-1 small">
                                        Writing comment as "<a href="/dashboard.php"><?=$_SESSION['username']?></a>"
                                    </p>
                                    <button type="submit" name="commentButton" class="btn btn-sm btn-info mt-2 float-right">Submit</button>
                                    </form>
                                </div>
                                <div class="col-sm-12 p-3 mb-1 comment">
<?php
$stmt = $pdo->prepare ("SELECT * from comments where upload_id = :upid order by comment_date desc");
$stmt->execute (array(':upid'=> $_GET['upload_id'] ));

while( $comment= $stmt->fetch(PDO::FETCH_ASSOC)){

?>









                                    <div class="card p-3" >
                                    <form action="edit.php?case=6&comment_id=<?=$comment['comment_id']?>" method="post" class="comments-form" style="display:none" id="editComment<?=$comment['comment_id']?>">
                                                        <div class="input-group">
                                                            <textarea name="commentText" class="form-control" aria-label="textarea" style="resize:none" placeholder="Write comment here..."><?= str_replace( '<br />', "\n", $comment['comment'])?></textarea>
                                                        </div>
                                    </p>


                                                        <button type="submit" name="commentButton" class="btn btn-sm btn-info mt-2 float-right">Submit</button>
                                </form>

                                                    <p id="commentData<?=$comment['comment_id']?>" style=""> <?=$comment['comment']?></p>
                                        <footer>
                                        <span class="mt-3 float-left">
                                        <span class="small">Added by: <strong><?=getUsername($comment['commented_by'])?></strong></span><br>
                                        <span class="small">Date: <strong><?= explode(' ', $comment['comment_date'])[0] ?></strong></span>
                                    </span>
                                            <span class="small">
                                            <?php if($_SESSION['user_id'] == $comment['commented_by']){ ?>
<span class="clearfix"></span>
                                            <div class="table-data-feature mt-3 float-right">
                                                        <button class="item" title="Edit" name="editComment" id="commentEditBtn<?=$comment['comment_id']?>">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>

                                                        <a id="deleteComment<?=$comment['comment_id']?>" href="#"><button class="item" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button></a>
                                                    </div>
                                            <?php } ?>
                                            </span>
<script type="text/javascript">
$('a#deleteComment<?=$comment['comment_id']?>').click(function(e){
  e.preventDefault();
  $.ajax({
    url: "delete.php?case=7&comment_id=<?=$comment['comment_id']?>",
    success: function(){
      location.reload();
    }
  });
});
</script>
                                        </footer>
                                    </div>
                                    <script>
                                    $('#commentEditBtn<?=$comment['comment_id']?>').click(function(){
                                            $('#commentData<?=$comment['comment_id']?>').toggle();
                                            $('#editComment<?=$comment['comment_id']?>').toggle();
                                        });

                                    </script>
<?php } ?>
</div>


                            </div>
                        </section>
                        <div style="min-height: 100px">

    </div>
                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <?php
require "footer.php";
?>
