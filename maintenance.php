<?php
$page="Maintenance";
require "header.php";

if(!in_array('Edit Maintenance Mode',$_SESSION['permissions'])){
    header('location: dashboard.php');
}

require "sidebar.php";


?>

        <!-- PAGE CONTAINER-->
        <div class="page-container">

            <!-- MAIN CONTENT-->
            <div class="main-content container">
                <div class="section__content section__content--p30">
 <?php
if( isset($_SESSION["error"])){   echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';  unset($_SESSION["error"]); }
if( isset($_SESSION["success"])){   echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';  unset($_SESSION["success"]); }
// print_r($_SESSION['permissions'])

?>
                    <!-- Content goes here -->
    <!-- <span aria-hidden="true">&times;</span> -->
  <!-- </button> -->
</div>
<?php
$stmt= $pdo->prepare("select * from maintenance where is_active=1");
$stmt->execute();
$row= $stmt->Fetch(PDO::FETCH_ASSOC);

?>
                    <div class="pt-3 pb-3 bg-light">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <div class="table-data__tool">
                                <div class="table-data__tool-left mb-3"><h3 class="title-4">Maintenance Mode</h3></div>

                                </div>
                                <!-- <div class="collapse" id="collapseExample"> -->
                                <div class="card card-body small">
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <form action="maintain.php" method="post" enctype="multipart/form-data" class="form-horizontal" id="maintenanceMode">

                                                  <div class="form-row">
                                                    <div class="col-sm-6 form-group">
                                                            <label for="text-input" class="h6 form-control-label">Maintenance Active:</label>

                                                            <?=$row['is_active']? '<input type="checkbox" id="maintain" name="maintainActive" value="1" class="" checked>' : '<input type="checkbox" id="maintain" name="maintainActive" value="1" class="" >'?>
                                                    </div>
                                                  </div>
                                                  <div class="form-row">
                                                    <div class="col-sm-6 form-group">
                                                            <label for="password-input" class="h6 form-control-label">Message for users</label>
                                                            <textarea name="maintainMessage" class="form-control" ><?=$row['maintenance_message']?></textarea>
                                                    </div>
`
                                                  </div>

                                          <div class="row form-group">

                                                          <input type="submit" class="btn btn-primary btn-sm m-3" value="Update" name = "updateMaintain">


                                                      <a href="dashboard.php" class="btn btn-secondary btn-sm m-3">Cancel</a>


                                                  </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                                </div>

                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>














<?php

require "footer.php";

?>
