<?php

require "pdo.php";

session_start();


function userExists($userName){
    require "pdo.php";
    $stmt = $pdo->prepare ('SELECT username from user where username = :un');
    $stmt->execute(array( ':un' => $userName));
    $row= $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(empty($row)){
        return true;
    }
    return false;

}

function clientExists($clientName){
    require "pdo.php";
    $stmt = $pdo->prepare ('select client_name from clients where client_name = :cn');
    $stmt->execute(array( ':cn' => $clientName));
    $row= $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(empty($row)){
        return true;
    }
    return false;

}
function roleExists($roleName){
    require "pdo.php";
    $stmt = $pdo->prepare ('select role_name from role where role_name = :ro');
    $stmt->execute(array( ':ro' => $roleName));
    $row= $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(empty($row)){
        return true;
    }
    return false;

}

switch ($_GET['case']){

    case "1":

        if(userExists($_POST['newusername'])){

            if(isset($_POST['newusername']) && isset($_POST['password']) && strlen($_POST['newusername'])>0 && strlen($_POST['password'])>0 && isset($_POST['password']) && isset($_POST['password-confirm']) && strlen($_POST['password-confirm'])>0 && $_POST['role'] != '0'){

                if($_POST['password']== $_POST['password-confirm']){

                    $stmt = $pdo->prepare ('INSERT into user (username,password,role_id) values (:us, :pw, :ro) ');

                    $stmt->execute(array( ':us' => $_POST['newusername'],

                     ':pw' => base64_encode($_POST['password']),

                      ':ro' => $_POST['role'] ));

                      $_SESSION["success"] = "User successfully added.";

                    // header("Location: dashboard.php");

                    // return;

                    // $row = $stmt->fetch(PDO::FETCH_ASSOC);



                } else{

                    $_SESSION["error"] = "Passwords don't match.";

                    // header("Location: dashboard.php");

                    // return;

                }

            } else{

                $_SESSION["error"] = "All Fields are required.";

                // header("Location: dashboard.php");

                // return;

            }

        } else{

            $_SESSION["error"] = "Username is already taken. Please try another username.";

            // header("Location: dashboard.php");

            // return;

        }

        break;



    case "2":

        if(clientExists($_POST['nameClient'])){

            if(isset($_POST['nameClient']) && strlen($_POST['nameClient'])>0 ){

                if(isset($_POST['isVendor'])){

                    $stmt = $pdo->prepare ('INSERT into clients (is_vendor, client_name) values (1,:cn) ;');

                    $stmt->execute(array( ':cn' => $_POST['nameClient'] ));

                      $_SESSION["success"] = "Vendor successfully added.";



                    // header("Location: dashboard.php");

                    // return;



                } else{

                    $stmt = $pdo->prepare ('INSERT into clients (client_name) values (:cn) ');

                    $stmt->execute(array( ':cn' => $_POST['nameClient'] ));



                      $_SESSION["success"] = "Client successfully added.";

                    //   header("Location:".$_POST['page']);

                    //   return;

                }



            }else{

                $_SESSION["error"] = "All Fields are required.";

                // header("Location:".$_POST['page']);

                    // return;

            }

        } else{
            $_SESSION["error"] = "Client already Exists";

        }

        break;



    case "3":

        if(roleExists($_GET['roleName'])){

            if(isset($_GET['roleName']) && strlen($_GET['roleName'])>0 ){

                $stmt = $pdo->prepare ('INSERT into role (role_name,is_internal_user) values ( :rn,1) ');

                $stmt->execute(array( ':rn' => $_GET['roleName'],
                ':ii' => $_POST['isInternalUser']));

                $role_id = $pdo->lastInsertId();

                if (isset($_POST['permission'])){

                    foreach ($_POST['permission'] as $permissionId){

                        $stmt = $pdo->prepare ('INSERT into role_2_permission (role_id, permission_id) values (:rid , :pid) ');

                        $stmt->execute(array( ':rid' => $role_id, 'pid' => $permissionId ));

                    }

                }



                $_SESSION["success"] = "Role successfully added.";

                header("Location: roles.php");

                // return;



        } else{

            $_SESSION["error"] = "All Fields are required.";

            header("Location: roles.php");

            // return;

            }

        } else{
            $_SESSION["error"] = "Role Already Exists";

        }

        break;



    case "4":

        if(userExists($_POST['newusername'])){

            if(isset($_POST['newusername']) && isset($_POST['password']) && strlen($_POST['newusername'])>0 && strlen($_POST['password'])>0 && isset($_POST['password']) && isset($_POST['password-confirm']) && strlen($_POST['password-confirm'])>0 && isset($_GET['client_id'])){

                if($_POST['password']== $_POST['password-confirm']){

                    $stmt = $pdo->prepare ('INSERT into user (username,password,role_id) values (:us, :pw, :ro);');

                    $stmt->execute(array( ':us' => $_POST['newusername'],

                    ':pw' => base64_encode($_POST['password']),
                     ':ro' => $_GET['r'] ));





                    $userId = $pdo->lastInsertId();



                    $stmt = $pdo->prepare ('INSERT into user_2_client (user_id,client_id) values (:uid, :cid);');

                    $stmt->execute(array( ':uid' => $userId , ':cid'=> $_GET['client_id']));

                    $_SESSION["success"] = "User successfully added.";

                    // header("Location:dashboard.php");

                    // return;

                    // $row = $stmt->fetch(PDO::FETCH_ASSOC);



                } else{

                    $_SESSION["error"] = "Passwords don't match.";

                    // header("Location:dashboard.php");

                    // return;

                }

            } else{

                $_SESSION["error"] = "All Fields are required.";

                // header("Location:dashboard.php");

                    // return;

            }

        } else{

            $_SESSION["error"] = "Username is already taken. Please try another username.";

            // header("Location:dashboard.php");

                    // return;

        }

        break;



    case "5":

        if(isset($_POST['commentText'])){

            if(isset($_POST['commentText']) && strlen($_POST['commentText'])>1){

                $stmt = $pdo->prepare ('INSERT into comments (comment, commented_by,upload_id) values (:com, :uid, :upid);');

                $stmt->execute(array( ':uid' => $_SESSION['user_id'] , ':com'=> nl2br($_POST['commentText']), ':upid'=> $_GET['upload_id']) );

                $_SESSION["success"] = "User successfully added.";

                    // header("Location:".$_SESSION['present']);

                    // return;

            }

        }

        $_SESSION["error"] = "Could Not Add Comment";

            // header("Location:".$_SESSION['present']);

                    // return;



    break;



}



?>
