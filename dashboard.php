<?php
// require "pdo.php";
// session_start();
$page="Internal Users";
if(isset($_SESSION['client_id'])){
    header('Location:client.php?client_id='.$_SESSION['client_id']);
    return;
}
require "header.php";


require "sidebar.php";
?>


        <!-- PAGE CONTAINER-->
        <div class="page-container">

            <!-- MAIN CONTENT-->
            <div class="main-content container">
                <div class="section__content section__content--p30">
 <?php
if( isset($_SESSION["error"]) && $_SESSION["error"] != 0){   echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';  $_SESSION["error"]=0; }
if( isset($_SESSION["success"])){   echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';  unset($_SESSION["success"]); }
// print_r($_SESSION['permissions'])

?>
                    <!-- Content goes here -->
    <!-- <span aria-hidden="true">&times;</span> -->
  </button>
</div>
                    <div class="pt-3 pb-3 bg-light">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <div class="">
                                <h3 class="title-4">Users
                                  <?php
                                  if(in_array("Add User",$_SESSION['permissions'])){
                                      ?>
                                      <button class="btn btn-success btn-sm float-right" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                          <i class="zmdi zmdi-plus"></i>Add User</button>
                                  <?php } ?>
                                </h3>
                                </div>
                                <div class="collapse" id="collapseExample">
                                <div class="card card-body small">

                                    <h5 class="h5 mb-3">Add user</h5>
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <form action="add.php?case=1" method="post" enctype="multipart/form-data" class="form-horizontal" id="addUserForm">
                                                  <!-- <div class="row form-group">
                                                      <div class="col col-md-3">
                                                          <label class=" form-control-label">Add User</label>
                                                      </div>
                                                  </div> -->

                                                  <div class="form-row">
                                                    <div class="col-sm-6 form-group">
                                                            <label for="text-input" class=" form-control-label">Username</label>

                                                            <input type="text" id="text-input" name="newusername" class="form-control" required>
                                                    </div>

                                                    <div class="col-sm-6 form-group">

                                                            <label for="password-input" class=" form-control-label">Password</label>
                                                            <input type="password" id="pass" name="password" placeholder="" class="form-control" required>
                                                    </div>
                                                  </div>
                                                  <div class="form-row">
                                                    <div class="col-sm-6 form-group">
                                                            <label for="password-input" class=" form-control-label">Confirm Password</label>
                                                            <input type="password" id="cpass" name="password-confirm" placeholder="" class="form-control" required>
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                            <label for="select" class=" form-control-label">User Type</label>

                                                            <select name="role" id="select" class="form-control" required>
                                                                <option value="0">Please select</option>
                                                                <script type="text/javascript">
                                                                  $('#addUserForm').submit(function(e){
                                                                    if($('#cpass').val() != $('#pass').val()){
                                                                      alert("Password doesn't match.");
                                                                      e.preventDefault();
                                                                    }
                                                                    else{
                                                                      $(this).submit();
                                                                    }
                                                                  })
                                                                </script>
        <?php
        $stmt = $pdo->prepare ("SELECT role_name, role_id from role where is_internal_user = '1';");
        $stmt->execute ();
        // $row= db("SELECT role_name, role_id from role where is_internal_user = '1';");
        while ($row=$stmt->fetch(PDO::FETCH_ASSOC) ){
            echo ("<option value='".$row['role_id']."'>".$row['role_name']."</option>");
        }

        ?>
                                                            </select>
                                                    </div>
                                                  </div>

                                          <div class="row form-group">

                                                          <input type="submit" class="btn btn-primary btn-sm m-3" value="Submit" name = "adduser">


                                                      <button class="btn btn-secondary btn-sm m-3" type="button" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Cancel</button>


                                                  </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead class="thead-light">
                                            <tr class="">
                                                <th>Username</th>
                                                <th>Password</th>
                                                <th>Type</th>
                                                <th>Options</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$stmt = $pdo->prepare ("SELECT u.user_id ,u.username, u.password, r.role_name , r.is_internal_user from user as u LEFT JOIN role as r on u.role_id= r.role_id;");
$stmt->execute ();

while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    // print_r ($_SESSION['permissions']);
    if($row['is_internal_user'] == 1){
        echo '<tr class="tr-shadow"><td>'.$row["username"].'</td>';

        if(in_array("View Admin Users Passwords",$_SESSION['permissions'])){
            if($row["role_name"] == 'Administrator') {   echo ('<td><span >'.base64_decode($row["password"]).'</span></td>');  }
            else if(in_array("View Internal Users Passwords",$_SESSION['permissions'])){
                echo ('<td><span >'.base64_decode($row["password"]).'</span></td>');
            } else {
                echo ('<td><span>********</span></td>');      }

        }else {
            if(in_array("View Internal Users Passwords",$_SESSION['permissions']) && $row["role_name"] != 'Administrator'){
                echo ('<td><span>'.base64_decode($row["password"]).'</span></td>');
            } else {
                echo ('<td><span >********</span></td>');      }

        }

        echo '<td>'.$row["role_name"].'</td>';


        echo '<td><div class="table-data-feature">';
        if(in_array("Edit User",$_SESSION['permissions'])){
            ?>
            <button type="button" class="item editToggle editScroll" data-toggle="collapse" href="#uedit<?=$row["user_id"]?>" role="button" aria-expanded="false" aria-controls="uedit<?=$row["user_id"]?>" data-placement="top" title="Edit"><i class="zmdi zmdi-edit" ></i></button>
            <!-- <script type="text/javascript">
              $('.editScroll').click(function(){
                // currentScrollPos=$(window).scrollTop();
                // $(window).scrollTop(1000);
                $('html, body').animate({
                    scrollTop: $("#uedit<?=$row["user_id"]?>").offset().top
                }, 1000);
              });
            </script> -->
        <?php
        }
        if(in_array("Remove User",$_SESSION['permissions'])){
            ?>
            <button type="button" class="item" data-placement="top" title="Delete" data-toggle="modal" data-target="#<?=$row["user_id"]?>Modal"><i class="zmdi zmdi-delete" ></i></button>
            <?php

        }

        ?>
        </div></td>
        <tr class="collapse" id="uedit<?=$row["user_id"]?>">
            <td class="tr-shadow" colspan="4">

                <h5 class="h5 mb-3">Edit user</h5>

            <form action="edit.php?case=1&user_id=<?=$row["user_id"]?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <?php
$st = $pdo->prepare ("SELECT username, password, role_id from user where user_id= $row[user_id];");
$st->execute ();
// $row= db("SELECT role_name, role_id from role where is_internal_user = '1';");
$edit=$st->fetch(PDO::FETCH_ASSOC) ;
?>
                                                  <div class="form-row">
                                                    <div class="col-sm-6 form-group">
                                                            <label for="text-input" class=" form-control-label">Username</label>

                                                            <input type="text" id="text-input" name="editusername" value="<?=$edit['username']?>" class="form-control">
                                                    </div>

                                                    <div class="col-sm-6 form-group">

                                                            <label for="password-input" class=" form-control-label">Password</label>
                                                            <input type="password" id="password-input" name="password" value="<?=base64_decode($edit['password'])?>" class="form-control">
                                                    </div>
                                                  </div>
                                                  <div class="form-row">
                                                    <div class="col-sm-6 form-group">
                                                            <label for="password-input" class=" form-control-label">Confirm Password</label>
                                                            <input type="password" id="password-input" name="password-confirm" value="<?=base64_decode($edit['password'])?>" class="form-control">
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                            <label for="select" class=" form-control-label">User Type</label>

                                                            <select name="role" id="select" class="form-control">
                                                                <option value="0">Please select</option>
        <?php
$stm = $pdo->prepare ("SELECT role_name, role_id from role where is_internal_user = '1';");
$stm->execute ();
// $row= db("SELECT role_name, role_id from role where is_internal_user = '1';");
while ($data=$stm->fetch(PDO::FETCH_ASSOC) ){
    if($data['role_id'] == $edit['role_id']){ echo ("<option value='".$data['role_id']."' selected>".$data['role_name']."</option>");}
    else { echo ("<option value='".$data['role_id']."'>".$data['role_name']."</option>");}
}

        ?>
                                                            </select>
                                                    </div>
                                                  </div>

                                          <div class="row form-group">
                                          <div class="col-sm-6 form-group">
                                          <input type="submit" class="btn btn-primary btn-sm" value="Update" name = "edituser">


                                          <button class="btn btn-secondary btn-sm" type="button" data-toggle="collapse" href="#uedit<?=$row["user_id"]?>" role="button" aria-expanded="false" aria-controls="collapseExample">Cancel</button>

                                                </div>
                                                  </div>
                                      </form>


    </td>
    </tr></tr>
        <?php
    }
?>





<!-- Modal-->
<div class="modal fade" id="<?=$row["user_id"]?>Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to delete <?= $row["username"] ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Click confirm to Delete.
      </div>
      <div class="modal-footer">
      <a href="delete.php?case=1&user_id=<?=$row["user_id"]?>" ><button type="button" class="btn btn-primary">Confirm</button></a>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>


<?php

}
?>



                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                        <div class="row">


</div>

                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>




<?php
require "footer.php";
?>
