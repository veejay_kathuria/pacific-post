
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <!-- <script src="vendor/circle-progress/circle-progress.min.js"></script> -->
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <!-- <script src="vendor/chartjs/Chart.bundle.min.js"></script> -->
    <!-- <script src="vendor/select2/select2.min.js"> -->
    <!-- </script> -->
<script src="js/custom.js"></script>
    <!-- Main JS-->
    <script src="js/main.js"></script>
    <!-- <script type="text/javascript">
    $('.editToggle').click(function(){
        $('.collapse').removeClass('show');
    })
    </script> -->
    <script>

      $(document).ready(function(){
        setTimeout(function() {

            $('.alert').fadeOut('ease-out');

        }, 3000);
      });
    </script>
</body>

</html>
<!-- end document-->
