<?php
// require "pdo.php";
// session_start();

// if( !isset($_GET['action']) || strlen($_GET['action'])<1){
//     $_SESSION["error"] = "Role name is required.";
//     header("Location: roles.php");
//     return;

// }


require "header.php";


require "sidebar.php";


function  ispermitted($permissionid){
    if ($_GET['action'] == 'edit'){
        require "pdo.php";


        $stm = $pdo->prepare ("SELECT role_id, permission_id from role_2_permission where role_id = :ro ;");
        $stm->execute ( array(':ro'=> $_GET['role_id']) );
        while($row = $stm->fetch(PDO::FETCH_ASSOC)){
            if($row['permission_id'] == $permissionid) {
                return true;
            }
        }

    }
    return false;
}
?>


        <!-- PAGE CONTAINER-->
        <div class="page-container">

            <!-- MAIN CONTENT-->
            <div class="main-content container">
                <div class="section__content section__content--p30">
 <?php
if( isset($_SESSION["error"])){   echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';  unset($_SESSION["error"]); }
if( isset($_SESSION["success"])){   echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';  unset($_SESSION["success"]); }
// print_r($_SESSION['permissions'])




?>
                            <div class="table-headhing__tool">
                            <?php if ($_GET['action'] == 'edit'){ ?>
                                <h3 class="h3 mb-3">Role Details : <?=$_GET['roleName']?></h3>
<?php } else{?>
                                <h3 class="h3 mb-3">Role Details : <?=$_POST['roleName']?></h3>
<?php } ?>


                                </div>
                    <!-- Content goes here -->
    <!-- <span aria-hidden="true">&times;</span> -->
  <!-- </button> -->
<!-- </div> -->
<?php if ($_GET['action'] == 'edit'){ ?>
<form action = 'edit.php?roleName= <?=urlencode($_GET['roleName'])?>&case=3&role_id= <?=urlencode($_GET['role_id'])?>'  method= 'post' class="rolePermissions">
<?php } else{?>
    <form action = 'add.php?case=3&roleName= <?=urlencode($_POST['roleName'])?>'  method= 'post' class="rolePermissions">
<?php } ?>

<?php
$stmt = $pdo->prepare ("SELECT * from privilege;");
$stmt->execute ();
// $row= db("SELECT role_name, role_id from role where is_internal_user = '1';");
// $privilege=$stmt->fetchAll(PDO::FETCH_ASSOC);
// print_r ($privilege);
while ($privilege = $stmt->fetch(PDO::FETCH_ASSOC) ){

?>
                            <div class="col-md-12 p-3 bg-light">
                                <!-- DATA TABLE -->



                                <div class="table-responsive table-responsive-data2">


                            <div class="table-data__tool">
                                <div class="table-data__tool-left"><h3 class="title-5 mb-2"><?= $privilege['privilege_name']?></h3></div>
                                </div>

                                    <table class="table table-data2">
                                        <thead class="thead-light">
                                            <tr class="text-light">
                                                <th class="th-blank"></th>
                                                <th class="th-permission">Permission</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$stm = $pdo->prepare ("SELECT * from permission where privilege_id= ".$privilege['privilege_id'].";");
$stm->execute ();

while($row = $stm->fetch(PDO::FETCH_ASSOC)){
    // print_r ($_SESSION['permissions']);
?>
                                        <tr class="tr-shadow">
                                        <td>
                                                    <label class="au-checkbox">

                                                        <input type="checkbox" name="permission[]" value="<?= $row['permission_id']?>" <?php echo ispermitted($row['permission_id']) ? "checked" : "" ?>>
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td>

                <td><span class=""><?= $row['permission_name']?></span></td>

                <td><span class=""><?= $row['permission_description']?></span></td>

<?php
}
?>

                                        </tbody>
                                    </table>
                                </div>

                                <!-- END DATA TABLE -->
                            </div>
<?php
}
?>
<div class="container">
                                                    <label class="form-check-label">
                                                    <input type='hidden' value='NULL' name='isInternalUser'>
                                                    <?php
                                                    if(isset($_GET['internal'])&& $_GET['internal'] !== '0'){
                                                    ?>

                                                        <input type="checkbox" name="isInternalUser" value="1" checked style="height: 15px;width: 15px;margin-top: 15px">
                                                        <?php
                                                    } else { ?>

                                                        <input type="checkbox" name="isInternalUser" value="1" style="height: 15px;width: 15px;margin-top: 15px">
                                                    <?php
                                                 }
                                                    ?>
                                                        Is Internal User?
                                                    </label>
</div>
<?php if ($_GET['action'] == 'edit'){ ?>
    <button type="submit" name= "updateRole" class="btn btn-success btn-md mt-3 mb-3" >Update</button>
    <a href="roles.php"><button type="Cancel" class="btn btn-dark btn-md mt-3 mb-3" >Cancel</button></a>

<?php } else {?>

    <button type="submit" name= "addRole" class="au-btn au-btn-icon au-btn--green au-btn--small mt-3 mb-3 " ><i class="zmdi zmdi-plus"></i> Add Role</button>
    <a href="roles.php"><button type="submit" class="btn btn-dark btn-md mt-3 mb-3" >Cancel</button></a>

<?php } ?>
</form>



                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>




<?php

require "footer.php";
?>
