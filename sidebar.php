<?php
// require "pdo.php";
// // session_start();
?>
 <!-- MENU SIDEBAR-->
       <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="dashboard.php">
                    <h3>
                        <img src="images/pacificPostLogo.jpg" width="80px"/>
                        Pacific Post
                    </h3>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <!-- <li>
                            <a class="" href="dashboard.php">
                                <i class="fas fa-tachometer-alt"></i>Internal Users</a>
                            <!-- <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="dashboard.php">Dashboard</a>
                                </li>

                            </ul> -->
                            <?php  if(in_array("Edit Maintenance Mode",$_SESSION['permissions']) || in_array("View Media Cleanup",$_SESSION['permissions']) || in_array("Edit Role",$_SESSION['permissions']) || in_array("Remove Role",$_SESSION['permissions']) || in_array("Add Role",$_SESSION['permissions'])) { ?>
                            <li class="has-sub">
                                <!-- <a class="js-arrow open" href="#">
                                <i class="fas fa-user"></i>Administration</a> -->
                                    <ul class="list-unstyled js-sub-list" style="">
                                    <div class="top-campaign mt-3">
                                    <div class="table-data__tool">
                                    <div class="table-data__tool-left"><h3 class="title-3 mb-2">Administration</h3></div>
                                    </div>
                                    <?php
                                        if(in_array("Edit Maintenance Mode",$_SESSION['permissions'])){
                                    ?>
                                    <div class="pl-3"><a href="maintenance.php" class="<?= $page=="Maintenance"?"active":""  ?>">Maintenance Mode</a></div>
                                    <?php
                                        }
                                        if(in_array("View Media Cleanup",$_SESSION['permissions'])){
                                    ?>
                                    <div class="pl-3"><a href="#">Media Cleanup</a></div>
                                    <?php
                                        }
                                        if(in_array("Add Role",$_SESSION['permissions'])){
                                    ?>
                                    <div class="pl-3"><a href="roles.php" class="<?= $page=="Roles"?"active":""  ?>">Roles</a></div>
                                    <div class="pl-3">
                                      <a class="<?= $page=="Internal Users"?"active":""  ?>" href="dashboard.php">
                                        Internal Users</a>
                                    </div>
    <?php } ?>
                                <?php

                                 ?>

                                    </ul>
                            </li>
                            <?php
                          }
                            ?>
                        <?php  if(in_array("View Clients List",$_SESSION['permissions'])) { ?>
                        <li class="has-sub">

                            <ul class="list-unstyled js-sub-list" style="display:block">
                                <!-- <li>
                                    <a href="index.html">Dashboard 1</a>
                                </li>
                                <li>
                                    <a href="index2.html">Dashboard 2</a>
                                </li>
                                <li>
                                    <a href="index3.html">Dashboard 3</a>
                                </li>
                                <li>
                                    <a href="index4.html">Dashboard 4</a>
                                </li> -->
                                <div class="top-campaign mt-3">
                                <div class="table-data__tool">
                                <div class="table-data__tool-left"><h3 class="title-3 mb-3">clients</h3></div>
                                <div class="table-data__tool-right">
                                    <?php
                                    if(in_array("Add Client",$_SESSION['permissions'])){ ?>
                                        <button class="btn btn-success btn-sm" data-toggle="collapse" href="#addClientDesktop" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <i class="zmdi zmdi-plus"></i>Add Client</button>
                                    <?php } ?>

                                </div>
                                </div>
                                <div class="collapse" id="addClientDesktop">
                                <div class="card card-body p-3">
                                <form action="add.php?case=2" method="post" enctype="multipart/form-data" class="form-horizontal" id="addClientForm">
                                            <div class="row">
                                                <div class="col col-md-">
                                                    <label class=" form-control-label h5">Add Client</label>
                                                </div>
                                            </div>
                                            <!-- <div class="row form-group"> -->


                                              <div class="condense">
                                                <div class="row col-md-12">
                                                    <label for="text-input" class=" form-control-label">Client Name</label>
                                                </div>
                                                <div class="row form-group col col-md-12">
                                                    <input type="text" id="text-input" name="nameClient"  class="form-control">
                                                    <!-- <small class="form-text text-muted"></small> -->
                                                </div>
                                            <!-- </div> -->
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="vendor-element" class=" form-control-label">Is a vendor?</label>
                                                </div>
                                                <div class="col-sm-6 mt-2">
                                                  <label class="au-checkbox">
                                                        <input type="checkbox" name="isVendor" class="form-control" value='2'>
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                    <!-- <input type="checkbox" name="isVendor" class="form-control" value='2'> -->
                                                    <!-- <small class="help-block form-text"></small> -->
                                                </div>
                                            </div>
                                              </div>


                                            <div class="row">
                                                <div class="col col-md-12 mt-1">
                                                    <input type="submit" class="btn btn-primary btn-sm" value="Submit" name = "addClient">
                                                    <button class="btn btn-secondary btn-sm" type="button" data-toggle="collapse" href="#addClientDesktop" role="button" aria-expanded="false" aria-controls="addClientDesktop">Cancel</button>

                                                </div>
                                            </div>

                                </form>

                                </div>
                                </div>
                                    <div class="table-responsive clientsTable">
                                        <table class="table table-top-campaign">
                                            <tbody>
                                              <?php
                                             $stmt = $pdo->prepare ("SELECT * from clients ;");
                                             $stmt->execute ();
                                             // $row= db("SELECT role_name, role_id from role where is_internal_user = '1';");
                                             while ($row=$stmt->fetch(PDO::FETCH_ASSOC) ){


                                             ?>
                                              <div class="col-12 well p-2 clientCard">
                                                <p class="float-left clientNameText text-dark">
                                                  <a id="clientFormBtn" href="client.php?client_id=<?=$row['client_id']?>&client_name=<?= $row['client_name']?>&r=<?=$row['is_vendor'] ? '2' : '22' ?>"><?= $row['client_name']?></a>
                                                </p>

                                                <span class="clearfix"></span>

                                                <div class="table-data-feature float-right">
                                                  <?php
                                              if(in_array("Edit Client",$_SESSION['permissions'])){
                                                  ?>

                                                  <button type="button" class="item collapsed editToggle" data-toggle="collapse" href="#editDesktop<?=$row["client_id"]?>" role="button" aria-expanded="false" aria-controls="edit<?=$row["client_id"]?>" data-placement="top" title="Edit"><i class="zmdi zmdi-edit" ></i></button>
                                                  <?php
                                              }

                                              if(in_array("Remove Client",$_SESSION['permissions'])){
                                                  ?>
                                                  <a href="delete.php?case=2&client_id=<?=$row["client_id"]?>" ><button type="button" class="item" ><i class="zmdi zmdi-delete" ></i></button></a>

                                              <?php } ?>
</div>
                                                    <form id="clientForm<?=$row['client_id']?>" class="clientpage" action="client.php" method="post"><input type ="hidden" name="client_id" value="<?=$row['client_id']?>">
                                                    <input type ="hidden" name="client_name" value="<?=$row['client_name']?>"></form>
                                                    <script>
$('#clientFormBtn<?=$row['client_id']?>').click(function(){
    $('#clientForm<?=$row['client_id']?>').submit();

});
</script>
                                                  <span class="clearfix"></span>
                                              </div>
                                              <div class="collapse bg-light p-2 clientCardAccordion" id="editDesktop<?=$row["client_id"]?>">
                                                <form action="edit.php?case=2&client_id=<?=$row["client_id"]?>" method="post" enctype="multipart/form-data" class="form-horizontal editClientForm<?php echo $row["client_id"] ?>">


                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <label class="h6 form-control-label">Edit Client</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row form-group">
                                                                                <div class="col-sm-12">
                                                                                    <label for="text-input" class=" form-control-label">Client Name:</label>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <input type="text" id="text-input" name="nameClient" value="<?=$row['client_name']?>" class="form-control" required>
                                                                                    <!-- <small class="form-text text-muted"></small> -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="row form-group">
                                                                                <div class="col-sm-6">
                                                                                    <label for="vendor-element" class=" form-control-label">Is a vendor?</label>
                                                                                </div>
                                                                                <div class="col-sm-2" style="padding-top: 8px;">
                                                                                    <?php
                                                                                    if($row ['is_vendor'] !== NULL){
                                                                                        ?>
                                                                                        <input type="checkbox" name="isVendor" class="form-control" value='1' checked>
                                                                                     <?php   } else{ ?>
                                                                                    <input type="checkbox" name="isVendor" class="form-control" value='1'>
                                      <?php } ?>
                                                                                    <!-- <small class="help-block form-text"></small> -->
                                                                                </div>
                                                                            </div>


                                                                            <span class="mr-2">
                                                                        <input type="submit" class="btn btn-primary btn-sm" value="Update" name = "editClient">
                                                                            <!-- <i class="fa fa-dot-circle-o"></i> Add User
                                                                        </button> -->
                                                                       <a href="#editDesktop<?=$row["client_id"]?>" data-toggle="collapse" style="display:inline-block"><button class="btn btn-dark btn-sm">Cancel</button></a>
</span>
                                                                </form>
                                                                <!-- <script type="text/javascript">
                                                                  $('.editClientForm').submit(function(e){
                                                                    e.preventDefault();
                                                                    formData=$(this).serialize();
                                                                    $.ajax({
                                                                      type: "POST",
                                                                      data: formData,
                                                                      url: "edit.php?case=2&client_id=",
                                                                      success: function(){
                                                                        location.reload();
                                                                      }
                                                                    })
                                                                  })
                                                                </script> -->
                                              </div>
                                              <tr class="collapse" id="edit<?=$row["client_id"]?>">
      <td class="tr-shadow" colspan="2">

</td>
</tr>

                                    </div>
                                </form>

    </td>
    </tr>




                                          </tr>

<div class="modal fade" id="client<?=$row["client_id"]?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
  <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to delete <?= $row["client_name"] ?></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  Click confirm to Delete.
</div>
<div class="modal-footer">
<a href="delete.php?case=2&client_id=<?=$row["client_id"]?>" ><button type="button" class="btn btn-primary">Confirm</button></a>

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

</div>
</div>
</div>
</div>

<?php
}
?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
<script>

// $("#addClientForm").submit(function(e) {

// e.preventDefault(); // avoid to execute the actual submit of the form.

// var form = $(this);
// var url = form.attr('action');

// $.ajax({
//        type: "POST",
//        url: url,
//        data: form.serialize(), // serializes the form's elements.
//        success: function(data)
//        {
//            alert(data); // show response from the php script.
//        }
//      });


// });




//     var selected = [];

// var vendor = $("#myselect").val();



// $('#addClient').click(function() {
//     $("input:checkbox[class= uploading]:checked").each(function() {
//        selected.push($(this).val());
//   });

//     if(vendor){
//         selected = JSON.stringify(selected);
//         $.ajax({
//                 type:"post",
//                 data: {
//                             vendor: vendor,
//                             uploads: selected
//                         },
//                 url: 'assign.php ',
//                 success: function(data){
//                     console.log("Sent "+ data);
//             }
//         });


//     }


// });

</script>
