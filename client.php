<?php
// require "pdo.php";
// session_start();

require "header.php";


if(!isset($_SESSION['client_id'])){
require "sidebar.php";
}

if(!isset($_SESSION['client_id'])){
    ?>
            <!-- PAGE CONTAINER-->
            <div class="page-container">
    <?php } else { ?>

            <div class= "container">

    <?php }   ?>


            <!-- MAIN CONTENT-->
            <div class="main-content container">
                <div class="section__content section__content--p30">
<?php
if( isset($_SESSION["error"])){   echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';  unset($_SESSION["error"]); }
if( isset($_SESSION["success"])){   echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';  unset($_SESSION["success"]); }
// print_r($_SESSION['permissions'])




?>
                    <!-- Content goes here -->
                    <?php if(in_array("View Users",$_SESSION['permissions'])) { ?>

                    <section>
                    <h4 class="h4 mb-3"><?=$_REQUEST['client_name']?></h4>
                        <div class="col-12 bg-light pt-3 pb-3">
                            <!-- DATA TABLE -->
                            <div class="table-data__tool pb-3">
                                <div class="table-data__tool-left"><h3 class="title-2">Users</h3></div>
                                <div class="table-data__tool-right">
                                    <?php
                                    if(in_array("Add User",$_SESSION['permissions'])){
                                        ?>
                                        <button class="btn btn-success btn-sm" data-toggle="collapse" href="#addUserCollapse" role="button" aria-expanded="false" aria-controls="addUserCollapse">
                                            <i class="zmdi zmdi-plus"></i>Add User</button>
                                    <?php } ?>
                                </div>
                                </div>
                                <div class="collapse" id="addUserCollapse">
                                <div class="card card-body">
                                <form action="add.php?case=4&client_id=<?=urldecode($_REQUEST['client_id'])?>&r=<?=$_GET['r']?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class="h5 form-control-label">User Details</label>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3 mt-2">
                                                    <label for="text-input" class=" form-control-label">Username</label>
                                                </div>
                                                <div class="col-9 col-md-6">
                                                    <input type="text" id="text-input" name="newusername" class="form-control" required>
                                                    <!-- <small class="form-text text-muted"></small> -->
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3 mt-2">
                                                    <label for="password-input" class=" form-control-label">Password</label>
                                                </div>
                                                <div class="col-9 col-md-6">
                                                    <input type="password" id="password-input" name="password" placeholder="" class="form-control" required>
                                                    <!-- <small class="help-block form-text"></small> -->
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3 mt-2">
                                                    <label for="password-input" class=" form-control-label">Confirm Password</label>
                                                </div>
                                                <div class="col-9 col-md-6">
                                                    <input type="password" id="password-input" name="password-confirm" placeholder="" class="form-control" required>
                                                    <!-- <small class="help-block form-text"></small> -->
                                                    </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-1">
                                                <input type="submit" class="btn btn-primary btn-sm" value="Submit" name = "adduser">
                                                </div>
                                                <div class="col col-md-1">
                                                <button class="btn btn-secondary btn-sm" type="button" data-toggle="collapse" href="#addUserCollapse" role="button" aria-expanded="false" aria-controls="addUserCollapse">Cancel</button>

                                                    </div>
                                            </div>
                                </form>
                                </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead class="thead-light">
                                            <tr class="text-light">
                                                <th>Username</th>
                                                <th>Password</th>
                                                <th>Type</th>
                                                <th>Options</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$stmt = $pdo->prepare ("SELECT u.user_id ,u.username, u.password from user as u LEFT JOIN user_2_client as u2c on u.user_id= u2c.user_id where client_id = :cid;");
$stmt->execute (array(':cid'=> $_REQUEST['client_id']));

while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    // print_r ($_SESSION['permissions']);
        ?>
        <tr id="users_panel" class="tr-shadow"><td><?=$row["username"]?></td>
        <?php
        if(in_array("View Users Passwords",$_SESSION['permissions'])){ ?>
            <td><span class=""><?= base64_decode($row["password"])?></span></td>
        <?php  }else{  ?>
            <td><span class="">&#42;&#42;&#42;&#42;&#42;&#42;&#42;&#42;&#42;&#42;</span></td>
        <?php   } ?>

        <td>Client</td>


        <td><div class="table-data-feature">
        <?php   if(in_array("Edit User",$_SESSION['permissions'])){            ?>
            <button type="button" class="item collapsed editToggle" data-toggle="collapse" href="#edit<?=$row["user_id"]?>" data-parent="#users_panel" role="button" aria-expanded="false" aria-controls="edit<?=$row["user_id"]?>" data-placement="top" title="Edit"><i class="zmdi zmdi-edit" ></i></button>


        <?php
        }
        if(in_array("Remove User",$_SESSION['permissions'])){

            ?>

<a href="delete.php?case=4&user_id=<?=$row["user_id"]?>" ><button type="button" class="item" data-placement="top" title="Delete" data-toggle="modal" data-target="#client<?=$row["user_id"]?>"><i class="zmdi zmdi-delete" ></i></button></a>
            <?php

        }

        ?>
        </div></td>
        <tr class="collapse" id="edit<?=$row["user_id"]?>">
            <td class="tr-shadow" colspan="4">
                <form action="edit.php?case=4&user_id=<?=$row["user_id"]?>&r=<?=$_GET['r']?>" method="post" enctype="multipart/form-data" class="form-horizontal">
<?php
$st = $pdo->prepare ("SELECT username, password from user where user_id= $row[user_id]");
$st->execute ();
// $row= db("SELECT role_name, role_id from role where is_internal_user = '1';");
$edit=$st->fetch(PDO::FETCH_ASSOC) ;
?>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class="h5 form-control-label">Edit User</label>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Username</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="text-input" name="editusername" value="<?=$edit['username']?>" class="form-control">
                                                    <!-- <small class="form-text text-muted"></small> -->
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="password-input" class=" form-control-label">Password</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="password" id="password-input" name="password" value="<?=base64_decode($edit['password'])?>" class="form-control">
                                                    <!-- <small class="help-block form-text"></small> -->
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="password-input" class=" form-control-label">Confirm Password</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="password" id="password-input" name="password-confirm" placeholder=""value="<?=base64_decode($edit['password'])?>" class="form-control">
                                                    <!-- <small class="help-block form-text"></small> -->
                                                    </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-1">
                                                    <input type="submit" class="btn btn-primary btn-sm" value="Update" name = "editUpload" />
                                                </div>
                                                <div class="col col-md-1">
                                                <button class="btn btn-secondary btn-sm" type="button" data-toggle="collapse" href="#edit<?=$row["user_id"]?>" role="button" aria-expanded="false" aria-controls="edit<?=$row["user_id"]?>">Cancel</button>

                                                    </div>
                                            </div>

                                </form>
    </td>
    </tr></tr>




<?php

}
?>



                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                        </div>
                    </section>
                    <?php } ?>
















































<?php if(in_array("View Client Media",$_SESSION['permissions']) || in_array("View Vendor Media",$_SESSION['permissions'])) { ?>

                    <section>
                        <div class="col-12 mt-3 bg-light mb-4 pb-3 pt-3">
                            <!-- DATA TABLE -->
                            <h3 class="title-2 pb-4">Media
                            <?php if(in_array("Upload Files",$_SESSION['permissions'])) { ?>

                                <button class="btn-success btn btn-sm float-right" data-toggle="collapse" href="#addMediaAccordion" role="button" aria-expanded="false" aria-controls="addMediaAccordion">
                                    <i class="zmdi zmdi-plus"></i> Add Media
                                </button>
                                <?php } ?>
                            </h3>
    <!-------  uploading ----->
                            <div class="collapse" id="addMediaAccordion">
                                <div class="card card-body">
                                    <p class="alert alert-info small">
                                    To upload a file, click the "Select Files" button and choose the file you wish to upload. When you are ready to start the upload process, click the "Upload Files" button.
                                    </p>
                                    <form class="mediaUploader" action="upload.php?case=1&client_id=<?=urldecode($_REQUEST['client_id'])?>" method="post" enctype="multipart/form-data">
                                    <div class="input-group small">

                                        <div class="custom-file">
                                            <input type="file" name="uploadedFile" class="form-control" id="inputGroupFile01" style="padding: 0px; border:0;" required>

                                        </div>

                                    </div>
                                    <code>Max File size should be <?php echo ini_get ('upload_max_filesize'); ?>.</code>
                                    <br>
                                    <br>
                                    <div class="row form-group mt-2">
                                                <div class="col-9 col-md-6">
                                                    <textarea name="addSubtitle" class="form-control"  placeholder="Description" style=""></textarea>
                                                    <!-- <small class="form-text text-muted"></small> -->
                                                </div>
                                    </div>
                                    <input type="hidden" name="clientPage" value="" >
                                    <input type="submit" name="uploadMedia" value="Upload" class="btn btn-info btn-sm mt-3">
                                    <input type="reset" name="" value="Cancel" class="btn btn-dark btn-sm mt-3" data-toggle="collapse" href="#addMediaAccordion" role="button" aria-expanded="false" aria-controls="addMediaAccordion">
                                        </form>
                                        <div id="uploadMediaProgBar" class="progress-bar mt-2 progress-bar-striped progress-bar-animated bg-primary" style="width: 100%; display: none;min-height: 20px;border-radius: 4px;display: none;"></div>
                                        <script type="text/javascript">
                                          $('input[name=uploadMedia]').click(function(){
                                            $('#uploadMediaProgBar').show();
                                          })
                                        </script>
                                </div>
                            </div>


                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead class="thead-light">
                                            <tr class="text-light">
                                            <th>
                                                    <label class="au-checkbox" style="top:2px!important">
                                                        <input type="checkbox" id="selectAllMedia">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th>
                                                <th class="th-filename">Filename</th>
                                                <th>Added</th>
                                                <th>Size</th>
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$flg=0;
$stmt = $pdo->prepare ("SELECT * from uploads as up LEFT JOIN upload_2_client as up2c on up.upload_id= up2c.upload_id where client_id = :cid and is_client_media is NULL ;");
$stmt->execute (array(':cid'=> $_REQUEST['client_id']));

while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
  $st = $pdo->prepare("select count(comment_id) from comments where upload_id= ".$row['upload_id']);
  $st->execute();
  $comments= $st->Fetch(PDO::FETCH_ASSOC);
    // print_r ($_SESSION['permissions']);
    $flg++;
    if( $row['is_active'] === NULL && !in_array("View Inactive Media",$_SESSION['permissions'])){ continue; }
 ?>
     <!-------  rows ----->

                                            <tr class="tr-shadow">
                                               <td >
                                                    <label class="au-checkbox">
                                                        <input type="checkbox" class="uploading" value="<?=$row['upload_id']?>">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td>
                                                <td style='<?php echo ($row['is_active'] == NULL)?"color: #b9b9b9":""; ?>'>
                                                <?php if($row['is_active'] !== NULL){ ?>
                                                    <p><a id="uploadPage<?=$row['upload_id']?>" href="comments.php?upload_id=<?=$row['upload_id']?>&client_id=<?=urldecode($_REQUEST['client_id'])?>"><?=$row['filename']?></a></p>

                                                <?php } else { ?>
                                                    <p><?=$row['filename']?></p>
                                                <?php } ?>
                                                    <small>Comments: <strong><?= $comments['count(comment_id)'] ?></strong> </small>
                                                </td>
                                                <td style='<?php echo ($row['is_active'] == NULL)?"color: #b9b9b9":""; ?>'>
                                                    <span class=""><?=$row['upload_date']?></span>
                                                </td>
                                                <td>
                                                    <code style='<?php echo ($row['is_active'] == NULL)?"color: #b9b9b9":""; ?>'><?=formatSizeUnits($row['upload_size'])?></code>
                                                </td>
                                                <td>
                                                    <div class="table-data-feature">

<?php if(in_array("Edit Media",$_SESSION['permissions'])){          ?>
    <button type="button" class="item" data-toggle="collapse" href="#upload<?=$row["upload_id"]?>" role="button" aria-expanded="false" aria-controls="upload<?=$row["upload_id"]?>" data-placement="top" title="Edit"><i class="zmdi zmdi-edit" ></i></button>


<?php
}
    if(in_array("Remove Media",$_SESSION['permissions'])){
?>
            <a class="deleteMedia" href="delete.php?case=5&upload_id=<?=urldecode($row["upload_id"])?>&client_id=<?=urldecode($_REQUEST['client_id'])?>"><button type="button" class="item" ><i class="zmdi zmdi-delete" ></i></button></a>


<?php
}
    if(in_array("Download Client Files",$_SESSION['permissions']) || in_array("Download All Files",$_SESSION['permissions'])){
?>
            <a href="uploads/<?=$row['filename']?>" download target="_blank"><button type="button" class="item" data-placement="top" title="Download"><i class="zmdi zmdi-download" ></i></button></a>
<?php
}
?>

                                                    </div>
                                                </td>
                                                <tr class="collapse" id="upload<?=$row["upload_id"]?>">
            <td class="tr-shadow" colspan="5">
            <form action="edit.php?case=5&upload_id=<?=urldecode($row["upload_id"])?>&client_id=<?=urldecode($_REQUEST['client_id'])?>" method="post" enctype="multipart/form-data" class="form-horizontal">


<div class="row form-group">
    <div class="col col-md-3">
        <label class="h5 form-control-label">Edit Media</label>
    </div>
</div>
<div class="row form-group">
    <div class="col col-md-3 mt-2">
        <label for="text-input" class=" form-control-label">Client Name:</label>
    </div>
    <div class="col-9 col-md-6">
    <select name="clientName" id="" class="custom-select pl-3 pr-3">
<?php
$stm = $pdo->prepare ("SELECT client_id,client_name from clients ;");
$stm->execute ();
while($ro=$stm->fetch(PDO::FETCH_ASSOC)){
if( $ro['client_id'] == $row['client_id']){
?>

<option value="<?= $ro['client_id']?>" selected><?=$ro['client_name']?></option>
<?php } else{
?>
<option value="<?= $ro['client_id']?>" ><?=$ro['client_name']?></option>
<?php
}
}
?>

</select>
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3 mt-2">
        <label for="text-input" class=" form-control-label">Description:</label>
    </div>
    <div class="col-9 col-md-6">
        <textarea name="editSubtitle" class="form-control"><?=$row['upload_subtitle']?></textarea>
        <!-- <small class="form-text text-muted"></small> -->
    </div>
</div>
<div class="row form-group">
    <div class="col col-md-3">
        <label for="vendor-element" class=" form-control-label">Make Active</label>
    </div>

    <div class="au-checkbox ml-3">
        <?php
        if($row ['is_active'] !== NULL){
            ?>
            <input type="checkbox" name="isActive" class="" value='1' checked>
         <?php   } else{ ?>
        <input type="checkbox" name="isActive" class="form-control" value='1'>
<?php } ?>
<span class="au-checkmark"></span>
        <!-- <small class="help-block form-text"></small> -->
    </div>
</div>


<div class="">
<!-- <input type="hidden" name="page" value='//$_SERVER['PHP_SELF']?>'> -->
<input type="submit" class="btn btn-primary btn-sm" value="Update" name = "editUpload">
<!-- <i class="fa fa-dot-circle-o"></i> Add User

</button> -->
<button class="btn btn-secondary btn-sm" type="button" data-toggle="collapse" href="#upload<?=$row["upload_id"]?>" role="button" aria-expanded="false" aria-controls="upload<?=$row["upload_id"]?>">Cancel</button>
</div>
</form>

    </td>
    </tr>


 </tr>

<?php } ?>
                                        </tbody>
                                    </table>

                                </div>
 <?php if(in_array("Assign to Vendor",$_SESSION['permissions']) && $flg>0 ){          ?>

                                <div class="well mt-2 mb-2 p-2 col-sm-6 col-md-6">
                                    <div class="input-group">
                                    <!-- <form method="post" action="assign.php"> -->
                                        <select name="assignVendor" id="myselect" class="form-control pl-3 pr-3" style="font-size:14px" required>
                                            <option value="" disabled selected>Choose a vendor</option>
<?php
$stmt = $pdo->prepare ("SELECT client_id,client_name from clients where is_vendor = '1';");
$stmt->execute ();
while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
?>
                                            <option value="<?= $row['client_id']?>"><?=$row['client_name']?></option>
<?php } ?>


                                        </select>
                                            <button id="assignMedia" class="btn btn-info btn-sm ml-2" type="button">Assign to Vendor</button>
                                        </form>
                                    </div>
                                </div>
 <?php } ?>
                                <!-- END DATA TABLE -->
                        </div>
                    </section>
<?php } ?>


<script type="text/javascript">

var selected = [];


$('#assignMedia').click(function() {
    $("input:checkbox[class= uploading]:checked").each(function() {
       selected.push($(this).val());
  });
  var vendor = $("#myselect").val();

//   alert(JSON.stringify(selected));

    if(vendor){
        selected = JSON.stringify(selected);
        $.ajax({
                type:"post",
                data: {
                            vendor: vendor,
                            uploads: selected
                        },
                url: 'assign.php?client_name=<?= $_GET['client_name'] ?>',
                success: function(data){
                    location.reload();
            }
        });


    }


});

</script>
































<?php if(in_array("View Client Uploads",$_SESSION['permissions'])) { ?>

<section>
    <div class="col-12 mt-3 bg-light mb-4 pb-3 pt-3">
        <!-- DATA TABLE -->
        <h3 class="title-2 pb-4">Client Uploads
        <?php if(in_array("Upload Files",$_SESSION['permissions'])) { ?>

            <button class="btn-success btn btn-sm float-right" data-toggle="collapse" href="#addClientMediaAccordion" role="button" aria-expanded="false" aria-controls="addClientMediaAccordion">
                <i class="zmdi zmdi-plus"></i> Add Media
            </button>
            <?php } ?>
        </h3>
<!-------  uploading ----->
        <div class="collapse" id="addClientMediaAccordion">
            <div class="card card-body">
                <p class="alert alert-info small">
                To upload a file, click the "Select Files" button and choose the file you wish to upload. When you are ready to start the upload process, click the "Upload Files" button.
                </p>
                <form class="mediaUploader" action="upload.php?case=2&client_id=<?=urldecode($_REQUEST['client_id'])?>" method="post" enctype="multipart/form-data">
                <div class="input-group small">

                    <div class="custom-file">
                        <input type="file" name="uploadedFile" class="form-control" id="inputGroupFile01" style="padding:0px; border:0;" required>
                    </div>

                </div>
                <code>Max File size should be <?php echo ini_get ('upload_max_filesize'); ?>.</code>
                <br>
                <div class="row form-group mt-2">
                            <div class="col-9 col-md-6">
                                <textarea name="addSubtitle" class="form-control" placeholder="Description" style=""></textarea>
                                <!-- <small class="form-text text-muted"></small> -->
                            </div>
                </div>
                <input type="submit" name="uploadMedia" value="Upload" class="btn btn-info btn-sm mt-3" id="uploadClientMedia">
                <input type="reset" name="" value="Cancel" class="btn btn-dark btn-sm mt-3" data-toggle="collapse" href="#addClientMediaAccordion" role="button" aria-expanded="false" aria-controls="addClientMediaAccordion">
                    </form>
                    <div id="uploadClientMediaProgBar" class="progress-bar mt-2 progress-bar-striped progress-bar-animated bg-primary" style="width: 100%; display: none;min-height: 20px;border-radius: 4px;display: none;"></div>
                    <script type="text/javascript">
                      $('input#uploadClientMedia').click(function(){
                        $('#uploadClientMediaProgBar').show();
                      })
                    </script>


            </div>
        </div>


            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead class="thead-light">
                        <tr class="text-light">

                            <th class="th-filename">Filename</th>
                            <th>Added</th>
                            <th>Size</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
$stmt = $pdo->prepare ("SELECT * from uploads as cup LEFT JOIN upload_2_client as cup2c on cup.upload_id= cup2c.upload_id where client_id = :cid and is_client_media= 1;");
$stmt->execute (array(':cid'=> $_REQUEST['client_id']));

while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
// print_r ($_SESSION['permissions']);
?>
<!-------  rows ----->

                        <tr class="tr-shadow">

                            <td>
                                <p><a href="comments.php?upload_id=<?=$row['upload_id']?>&client_id=<?=urldecode($_REQUEST['client_id'])?>"><?=$row['filename']?></a></p>
                                <small><?=$row['upload_subtitle']?></small>
                            </td>
                            <td>
                                <span class=""><?=$row['upload_date']?></span>
                            </td>
                            <td>
                                <code><?=formatSizeUnits($row['upload_size'])?></code>
                            </td>
                            <td>
                                <div class="table-data-feature">



<?php

if(in_array("Remove Media",$_SESSION['permissions'])){
?>
<a class="deleteMedia" href="delete.php?case=6&upload_id=<?=urldecode($row["upload_id"])?>&client_id=<?=urldecode($_REQUEST['client_id'])?>"><button type="button" class="item" ><i class="zmdi zmdi-delete" ></i></button></a>

<?php
}
if(in_array("Download Client Files",$_SESSION['permissions']) || in_array("Download All Files",$_SESSION['permissions'])){
?>
<a href="uploads/client_uploads/<?=$row['filename']?>" download target="_blank"><button type="button" class="item" data-placement="top" title="Download"><i class="zmdi zmdi-download" ></i></button></a>
<?php
}
?>

                                </div>
                            </td>

</tr>



<?php } ?>
                    </tbody>
                </table>

            </div>

            <!-- END DATA TABLE -->
    </div>
</section>
<?php } ?>




<?php if(in_array("View Assigned Vendor Media",$_SESSION['permissions'])) { ?>

<section>
    <div class="col-12 mt-3 bg-light mb-4 pb-3 pt-3">
        <!-- DATA TABLE -->
        <h3 class="title-2 pb-4">Assigned to Vendor
        </h3>

            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead class="thead-light">
                        <tr class="text-light">
                            <th>Company</th>
                            <th>Filename</th>
                            <th>Added</th>
                            <th>Assigned</th>
                            <th>Size</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
$stmt = $pdo->prepare ("SELECT * from uploads as cup LEFT JOIN assigned_2_vendor as a2v on cup.upload_id= a2v.upload_id where client_id = :cid ");
$stmt->execute (array(':cid'=> $_REQUEST['client_id']));

while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
// print_r ($_SESSION['permissions']);
?>
<!-------  rows ----->

                        <tr class="tr-shadow">
                            <td>
                                <span class=""><?=$row['assignee_name']?></span>
                            </td>
                            <td>
                                <p><a href="comments.php?upload_id=<?=$row['upload_id']?>&client_id=<?=urldecode($_REQUEST['client_id'])?>"><?=$row['filename']?></a></p>
                                <small><?=$row['upload_subtitle']?></small>
                            </td>
                            <td>
                                <span class=""><?=$row['upload_date']?></span>
                            </td>
                            <td>
                                <span class=""><?=$row['assigned_date']?></span>
                            </td>
                            <td>
                                <code><?=formatSizeUnits($row['upload_size'])?></code>
                            </td>
                            <td>
                                <div class="table-data-feature">



<?php

if(in_array("Download Client Files",$_SESSION['permissions']) || in_array("Download All Files",$_SESSION['permissions'])){
?>
<a href="uploads/client_uploads/<?=$row['filename']?>" download target="_blank"><button type="button" class="item" data-placement="top" title="Download"><i class="zmdi zmdi-download" ></i></button></a>
<?php
}
?>

                                </div>
                            </td>

</tr>



<?php } ?>
                    </tbody>
                </table>

            </div>

            <!-- END DATA TABLE -->
    </div>
</section>
<?php } ?>



                    <br>

                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>





<?php
require "footer.php";
?>
