-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2019 at 07:48 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pacificpost`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `is_vendor` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_name`, `is_vendor`) VALUES
(1, 'client1', NULL),
(2, 'client2', NULL),
(3, 'client3', NULL),
(4, 'client4', NULL),
(5, 'client5', NULL),
(7, 'vendor1', 1),
(8, 'client6', NULL),
(9, 'vendor2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `comment` varchar(1022) NOT NULL,
  `commented_by` int(11) NOT NULL,
  `comment_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `upload_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `comment`, `commented_by`, `comment_date`, `upload_id`) VALUES
(3, 'dfg', 1, '2019-03-20 12:12:20', 10),
(4, 'dfg', 1, '2019-03-20 12:12:20', 10),
(5, 'dfg', 1, '2019-03-20 12:12:20', 10),
(6, 'dfg', 1, '2019-03-20 12:12:20', 10);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL,
  `permission_name` text NOT NULL,
  `permission_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `privilege_id`, `permission_name`, `permission_description`) VALUES
(1, 1, 'Edit Maintenance Mode	', 'Ability to turn maintenance mode on and off'),
(2, 1, 'View Media Cleanup', 'Ability to run the media cleanup wizard'),
(3, 2, 'Add Client', 'Ability to add new clients'),
(4, 2, 'Edit Client', 'Ability to edit existing clients'),
(5, 2, 'Remove Client', 'Ability to remove clients (deletes all associated users and files)'),
(6, 3, 'Download Client Files', 'Ability to download files for users company only'),
(7, 3, 'Download All Files', 'Ability to download all files'),
(8, 4, 'Add Media', 'Ability to add new media'),
(9, 4, 'Remove Media', 'Ability to remove media'),
(10, 4, 'Edit Media', 'Ability to edit media'),
(11, 4, 'Upload Files', 'Ability to upload files (this is for clients)'),
(12, 4, 'Assign to Vendor', 'Ability to assign media to a vendor'),
(13, 5, 'Add Role', 'Ability to add new roles'),
(14, 5, 'Edit Role', 'Ability to edit roles'),
(15, 5, 'Remove Role', 'Ability to remove roles'),
(16, 6, 'Add User', 'Ability to add new users'),
(17, 6, 'Edit User', 'Ability to edit users'),
(18, 6, 'Remove User', 'Ability to remove users'),
(19, 7, 'View Clients List', 'Ability to see the list of clients'),
(20, 7, 'View Internal Users', 'Ability to see the list of internal users'),
(21, 7, 'View Users', 'Ability to see the list of users for clients'),
(22, 7, 'View Users Passwords', 'Ability to see user passwords in plain text'),
(23, 7, 'View Internal Users Passwords', 'Ability to see internal user passwords in plain text'),
(24, 7, 'View Admin Users Passwords', 'Ability to see admin passwords in plain text'),
(25, 7, 'View Client Media', 'Ability to see the list of client media files'),
(26, 7, 'View Client Uploads', 'Ability to see the list of client uploads'),
(27, 7, 'View Admin Tools', 'Ability to view the admin tools'),
(28, 7, 'View Only Own Company', 'Ability to see only the users company related media and etc.'),
(29, 7, 'View Inactive Media', 'Ability to see files flagged as inactive (invisible)\r\n'),
(30, 7, 'View Vendor Media', 'Ability for vendors to view files assigned to them (this is for vendors only)'),
(31, 7, 'View Assigned Vendor Media', 'Ability to view media assigned to vendors (this is for admins only)');

-- --------------------------------------------------------

--
-- Table structure for table `privilege`
--

CREATE TABLE `privilege` (
  `privilege_id` int(11) NOT NULL,
  `privilege_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privilege`
--

INSERT INTO `privilege` (`privilege_id`, `privilege_name`) VALUES
(1, 'admin'),
(2, 'clients'),
(3, 'downloads'),
(4, 'media'),
(5, 'roles'),
(6, 'users'),
(7, 'views');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` text NOT NULL,
  `is_internal_user` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `is_internal_user`) VALUES
(1, 'Administrator', 1),
(2, 'Vendor', NULL),
(7, 'Editor', 1),
(10, 'test', NULL),
(22, ' Client', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_2_permission`
--

CREATE TABLE `role_2_permission` (
  `r2p_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_2_permission`
--

INSERT INTO `role_2_permission` (`r2p_id`, `role_id`, `permission_id`) VALUES
(33, 2, 7),
(34, 2, 8),
(35, 2, 11),
(36, 2, 25),
(37, 2, 26),
(38, 2, 30),
(40, 7, 2),
(41, 7, 3),
(42, 7, 4),
(43, 7, 5),
(44, 7, 7),
(45, 7, 8),
(46, 7, 9),
(47, 7, 10),
(48, 7, 11),
(49, 7, 12),
(50, 7, 16),
(51, 7, 17),
(52, 7, 18),
(53, 7, 19),
(54, 7, 20),
(55, 7, 21),
(56, 7, 22),
(57, 7, 29),
(58, 7, 25),
(59, 7, 26),
(60, 7, 27),
(89, 1, 1),
(90, 1, 2),
(91, 1, 3),
(92, 1, 4),
(93, 1, 5),
(95, 1, 7),
(96, 1, 8),
(97, 1, 9),
(98, 1, 10),
(99, 1, 11),
(100, 1, 12),
(101, 1, 13),
(102, 1, 14),
(103, 1, 15),
(104, 1, 16),
(105, 1, 17),
(106, 1, 18),
(107, 1, 19),
(108, 1, 20),
(109, 1, 21),
(110, 1, 22),
(111, 1, 23),
(112, 1, 24),
(113, 1, 25),
(114, 1, 26),
(115, 1, 27),
(116, 1, 28),
(117, 1, 29),
(118, 1, 30),
(119, 1, 31),
(137, 22, 6),
(138, 22, 11),
(139, 22, 25),
(140, 22, 26),
(141, 22, 28);

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `upload_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `upload_subtitle` varchar(255) DEFAULT NULL,
  `upload_size` int(11) NOT NULL,
  `upload_type` varchar(255) NOT NULL,
  `upload_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`upload_id`, `filename`, `upload_subtitle`, `upload_size`, `upload_type`, `upload_date`, `user_id`) VALUES
(10, '1445634329600wh-1_hr.png', '', 134418, 'image/png', '2019-03-19 19:04:26', 1),
(11, '1433358097660-12c6 (bk)_med.png', 'dsfsfsdf', 274288, 'image/png', '2019-03-19 19:05:02', 1),
(12, '1433358084660-12c6_med.png', 'sdfsf', 323411, 'image/png', '2019-03-19 19:08:59', 1),
(13, '1433358097660-12c6 (bk)_med.png', 'adsad', 274288, 'image/png', '2019-03-19 19:09:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `upload_2_client`
--

CREATE TABLE `upload_2_client` (
  `u2c_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `is_active` int(1) DEFAULT NULL,
  `is_client_media` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upload_2_client`
--

INSERT INTO `upload_2_client` (`u2c_id`, `client_id`, `upload_id`, `is_active`, `is_client_media`) VALUES
(8, 7, 10, 1, NULL),
(9, 7, 11, 1, NULL),
(10, 7, 12, 1, NULL),
(11, 7, 13, 1, NULL),
(12, 1, 11, 1, NULL),
(13, 1, 12, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `role_id`, `username`, `password`, `created_at`) VALUES
(1, 1, 'admin', 'dGVzdGluZzEyMw==', '2019-03-14 14:23:54'),
(2, 2, 'vendor', 'dGVzdGluZzEyMw==', '2019-03-15 12:15:51'),
(6, 7, 'test1', 'dGVzdDEyMw==', '2019-03-15 15:47:35'),
(9, 1, 'test', 'dGVzdA==', '2019-03-15 15:51:51'),
(10, 0, 'client1', 'dGVzdDEyMw==', '2019-03-18 18:12:11'),
(11, 22, 'client2', 'dGVzdDEyMw==', '2019-03-18 18:12:11'),
(12, 22, 'client4', 'dGVzdDEyMw==', '2019-03-18 18:12:11'),
(13, 22, 'client4', 'dGVzdDEyMw==', '2019-03-18 18:12:11'),
(14, 0, 'vendor1_user', 'dGVzdDEy', '2019-03-18 19:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_2_client`
--

CREATE TABLE `user_2_client` (
  `user_2_client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_2_client`
--

INSERT INTO `user_2_client` (`user_2_client_id`, `user_id`, `client_id`) VALUES
(1, 10, 7),
(2, 11, 2),
(3, 12, 5),
(4, 13, 7),
(5, 14, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `privilege`
--
ALTER TABLE `privilege`
  ADD PRIMARY KEY (`privilege_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `role_2_permission`
--
ALTER TABLE `role_2_permission`
  ADD PRIMARY KEY (`r2p_id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `upload_2_client`
--
ALTER TABLE `upload_2_client`
  ADD PRIMARY KEY (`u2c_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_2_client`
--
ALTER TABLE `user_2_client`
  ADD PRIMARY KEY (`user_2_client_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `privilege`
--
ALTER TABLE `privilege`
  MODIFY `privilege_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `role_2_permission`
--
ALTER TABLE `role_2_permission`
  MODIFY `r2p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `upload_2_client`
--
ALTER TABLE `upload_2_client`
  MODIFY `u2c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_2_client`
--
ALTER TABLE `user_2_client`
  MODIFY `user_2_client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
