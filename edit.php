<?php
require "pdo.php";
session_start();

function userExists($userName){
    require "pdo.php";
    $stmt = $pdo->prepare ('SELECT username from user where username = :un');
    $stmt->execute(array( ':un' => $userName));
    $row= $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(empty($row)){
        return true;
    }
    return false;

}

function clientExists($clientName){
    require "pdo.php";
    $stmt = $pdo->prepare ('select client_name from clients where client_name = :cn');
    $stmt->execute(array( ':cn' => $clientName));
    $row= $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(empty($row)){
        return true;
    }
    return false;

}
function roleExists($roleName){
    require "pdo.php";
    $stmt = $pdo->prepare ('select role_name from role where role_name = :ro');
    $stmt->execute(array( ':ro' => $roleName));
    $row= $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(empty($row)){
        return true;
    }
    return false;

}



switch ($_GET['case']){
    case "1":
        if(isset($_POST['editusername'])){
            if(isset($_POST['editusername']) && isset($_POST['password']) && strlen($_POST['editusername'])>0 && strlen($_POST['password'])>0 && isset($_POST['password']) && isset($_POST['password-confirm']) && strlen($_POST['password-confirm'])>0 && $_POST['role'] != '0'){
                if($_POST['password']== $_POST['password-confirm']){
                    $stmt = $pdo->prepare ('UPDATE user set username= :us, password=:pw, role_id=:ro where user_id= :uid');
                    $stmt->execute(array( ':us' => $_POST['editusername'],
                     ':pw' => base64_encode($_POST['password']),
                      ':ro' => $_POST['role'],
                    ':uid' => $_GET['user_id'] ));
                    $_SESSION["success"] = "User successfully updated.";
                    // header("Location: dashboard.php");
                    // return;
                    // $row = $stmt->fetch(PDO::FETCH_ASSOC);

                } else{
                    $_SESSION["error"] = "Passwords don't match.";
                    // header("Location: dashboard.php");
                    // return;
                }
            } else{
                $_SESSION["error"] = "All Fields are required.";
                // header("Location: dashboard.php");
                // return;
            }
        } else{
            $_SESSION["error"] = "Invalid Entry";
            // header("Location: dashboard.php");
            // return;
        }
        break;

        case "2":
        // echo "I was here.";
        if(isset($_POST['nameClient'])){
            if(isset($_POST['nameClient']) && strlen($_POST['nameClient'])>0){
                if(isset($_POST['isVendor'])){
                    $stmt = $pdo->prepare ('update clients set is_vendor = 1, client_name= :cn where client_id= :cid ;');
                    $stmt->execute(array( ':cn' => $_POST['nameClient'], ':cid' => $_GET['client_id'] ));
                      $_SESSION["success"] = "Vendor successfully added.";
                      // header("Location:".$_POST['page']);
                    // return;

                } else{
                    $stmt = $pdo->prepare ('update clients set client_name= :cn where client_id= :cid ; ');
                    $stmt->execute(array( ':cn' => $_POST['nameClient'], ':cid' => $_GET['client_id'] ));

                      $_SESSION["success"] = "Client updated successfully.";
                    // header("Location:".$_POST['page']);
                    // return;
                }

            }else{
                $_SESSION["error"] = "All Fields are required.";
                // header("Location:". $_POST['page']);
                // return;
            }
        }
        break;

    case "3":
        if(isset($_GET['roleName'])){
            if(isset($_GET['roleName']) && strlen($_GET['roleName'])>0 && isset($_GET['role_id']) ){

              $stmt = $pdo->prepare ("UPDATE role set is_internal_user= :ii where role_id= :uid");
                $stmt->execute (array(':uid' => $_GET['role_id'] ,
                ':ii' => $_POST['isInternalUser'] ));

                $stmt = $pdo->prepare ("DELETE from role_2_permission where role_id= :uid");
                $stmt->execute (array(':uid' => $_GET['role_id'] ));
                if (isset($_POST['permission'])){
                    foreach ($_POST['permission'] as $permissionId){
                        $stmt = $pdo->prepare ('INSERT into role_2_permission (role_id, permission_id) values (:rid, :pid) ');
                        $stmt->execute(array( ':rid' => $_GET['role_id'], 'pid' => $permissionId ));
                    }
                }

                $_SESSION["success"] = "Role successfully updated.";
                header("Location: roles.php");
                // return;

        } else{
            $_SESSION["error"] = "All Fields are required.";
            header("Location: roles.php");
            // return;
        }
    }
        break;
    case "4":
        if(isset($_POST['editusername'])){
            if(isset($_POST['editusername']) && isset($_POST['password']) && strlen($_POST['editusername'])>0 && strlen($_POST['password'])>0 && isset($_POST['password']) && isset($_POST['password-confirm']) && strlen($_POST['password-confirm'])>0 && $_POST['role'] != '0'){
                if($_POST['password']== $_POST['password-confirm']){
                    $stmt = $pdo->prepare ('UPDATE user set username= :us, password=:pw, role_id=:ro where user_id= :uid');
                    $stmt->execute(array( ':us' => $_POST['editusername'],
                     ':pw' => base64_encode($_POST['password']),
                      ':ro' => $_POST['role'],
                    ':uid' => $_GET['user_id'] ));
                    $_SESSION["success"] = "User successfully updated.";
                    // header("Location:". $_SESSION['present']);
                    // return;
                    // $row = $stmt->fetch(PDO::FETCH_ASSOC);

                } else{
                    $_SESSION["error"] = "Passwords don't match.";
                    // header("Location:". $_SESSION['present']);
                // return;
                }
            } else{
                $_SESSION["error"] = "All Fields are required.";
                // header("Location:". $_SESSION['present']);
                // return;
            }
        } else{
            $_SESSION["error"] = "Invalid Entry";
            // header("Location:".$_SESSION['present']);
                // return;
            }
        break;


    case "5":

        // if(isset($_GET['upload_id'])){

            if(isset($_POST['isActive'])){
                $stmt = $pdo->prepare ('UPDATE uploads set upload_subtitle= :usub  where upload_id= :upid; update upload_2_client set client_id = :ncid, is_active = :ia where upload_id= :upid and client_id = :cid ;');
            $stmt->execute(array(':ncid' => $_POST['clientName'],
                     ':usub' => $_POST['editSubtitle'],
                     ':upid' => $_GET['upload_id'],
                      ':cid' => $_GET['client_id'],
                    ':ia' => $_POST['isActive']));
                    $_SESSION["success"] = " successfully updated.";

                    // header("Location:".$_SESSION['present']);
                    //   return;
            } else {
                $stmt = $pdo->prepare ('UPDATE uploads set upload_subtitle= :usub  where upload_id= :upid; update upload_2_client set client_id = :ncid ,is_active = NULL where upload_id= :upid and client_id = :cid ;');
            $stmt->execute(array( ':ncid' => $_POST['clientName'],
                    ':usub' => $_POST['uploadSubtitle'],
                     ':upid' => $_GET['upload_id'],
                      ':cid' => $_GET['client_id'] ));

                      $_SESSION["success"] = " successfully updated.";

                    //   header("Location:".$_SESSION['present']);
                    //   return;
            }


            // else{
            //     $_SESSION["error"] = "All Fields are required.";

            // }
        // }
        // $_SESSION["error"] = "Error Updating";

        // header("Location:". $_SESSION['present']);
                // ret?urn;
    break;


    case "6":
    if(isset($_POST['commentText'])){
        if(isset($_POST['commentText']) && strlen($_POST['commentText'])>0){
            $stmt = $pdo->prepare ('UPDATE comments set comment = :com where comment_id = :cid;');
            $stmt->execute(array( ':com'=> nl2br($_POST['commentText']), ':cid' => $_GET['comment_id']) );
            $_SESSION["success"] = "Comment successfully Updated.";
                // header("Location:".$_SESSION['present']);
                // return;
        }
    } else{
        $_SESSION["error"] = "Could Not Update Comment";
    }

        // header("Location:".$_SESSION['present']);
        //         return;
    break;

}

?>
