<?php


require "header.php";
if(!in_array("Add Role",$_SESSION['permissions'] || !in_array("Edit Role",$_SESSION['permissions']) || !in_array("Remove Role",$_SESSION['permissions']))){
    header('Location : index.php');
}

$page="Roles";
require "sidebar.php";
?>





        <!-- PAGE CONTAINER-->

        <div class="page-container">



            <!-- MAIN CONTENT-->

            <div class="main-content container">

                <div class="section__content section__content--p30">

 <?php

if( isset($_SESSION["error"])){   echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';  unset($_SESSION["error"]); }

if( isset($_SESSION["success"])){   echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';  unset($_SESSION["success"]); }

// print_r($_SESSION['permissions'])



?>

                    <!-- Content goes here -->

    <!-- <span aria-hidden="true">&times;</span> -->

  <!-- </button> -->

</div>

                    <div class="pt-3 pb-3 bg-light">

                            <div class="col-md-12">

                                <!-- DATA TABLE -->

                                <div class="table-data__tool mb-2">

                                <div class="table-data__tool-left"><h3 class="title-4">Roles</h3></div>

                                <div class="table-data__tool-right">

                                    <?php

                                    if(in_array("Add Role",$_SESSION['permissions'])){

                                        ?>

                                        <button class="btn btn-success btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">

                                            <i class="zmdi zmdi-plus"></i>Add Role</button>

                                    <?php } ?>

                                </div>

                                </div>

                                <div class="collapse" id="collapseExample">

                                <div class="card card-body">

                                <form action="permissions.php?action=Add" id="rolesAdd" method="post" enctype="multipart/form-data" class="form-horizontal">

                                            <!-- <div class="row form-group">

                                                <div class="col col-md-3">

                                                    <label class=" form-control-label">Add Role</label>

                                                </div>

                                            </div> -->

                                            <div class="row form-row">


                                                    <div class="col-sm-6" style="">
                                                      <label for="text-input" class=" form-control-label" style="margin-top:8px">New Role Name</label>


                                                      <input type="text" id="text-input" name="roleName" class="form-control">

                                                    </div>
                                            </div>
                                            <div class="form-row p-2">
                                              <input type="submit" class="btn btn-primary btn-sm mr-1" value="Submit" name = "addRole">
                                          <button class="btn btn-secondary btn-sm ml-1" type="button" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Cancel</button>

                                            </div>





                                </form>

                                </div>

                                </div>

                                <div class="table-responsive table-responsive-data2">

                                    <table class="table table-data2">

                                        <thead class="thead-dark">

                                            <tr class="text-light">

                                                <th>Roles</th>

                                                <th>Options</th>

                                            </tr>

                                        </thead>

                                        <tbody>

<?php

$stmt = $pdo->prepare ("SELECT * from role; ");

$stmt->execute ();



while($row = $stmt->fetch(PDO::FETCH_ASSOC)){ ?>



        <tr class="tr-shadow"><td><?=$row["role_name"]?></td>





        <td><div class="table-data-feature">

        <?php

        if(in_array("Edit Role",$_SESSION['permissions'])){

            ?>

            <a href="permissions.php?roleName=<?=urlencode($row['role_name'])?>&role_id=<?=urlencode($row['role_id'])?><?=($row['is_internal_user'] !== NULL)? '&internal='.urlencode($row['is_internal_user']) : '&internal=0'?>&action=edit")><button type="button" class="item" ><i class="zmdi zmdi-edit"></i> </button> </a>






        <?php

        }

        if(in_array("Remove Role",$_SESSION['permissions'])){

            ?>

            <button type="button" class="item" data-placement="top" title="Delete" data-toggle="modal" data-target="#role<?=$row["role_id"]?>"><i class="zmdi zmdi-delete" ></i></button>

            <?php



        }



        ?>

        </div></td>

        </tr>













<!-- Modal-->

<div class="modal fade" id="role<?=$row["role_id"]?>" tabindex="-1" role="dialog" aria-labelledby="collapseExample" aria-hidden="true">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to delete <?= $row["role_name"] ?></h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true">&times;</span>

        </button>

      </div>

      <div class="modal-body">

        Click confirm to Delete.

      </div>

      <div class="modal-footer">

      <a href="delete.php?case=3&role_id=<?=$row["role_id"]?>" ><button type="button" class="btn btn-primary">Confirm</button></a>

      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>



      </div>

    </div>

  </div>

</div>





<?php



}

?>







                                        </tbody>

                                    </table>

                                </div>

                                <!-- END DATA TABLE -->

                            </div>

                        </div>

                        <div class="row">





</div>



                </div>

            </div>

            <!-- END MAIN CONTENT-->

            <!-- END PAGE CONTAINER-->

        </div>



    </div>









<?php

require "footer.php";

?>
