<?php

require "pdo.php";

session_start();


$stm= $pdo->prepare("select * from maintenance");
        $stm->execute();
        $maintain= $stm->FetchAll(PDO::FETCH_ASSOC);
        $maintain = end($maintain);
        
        if( $maintain["is_active"]){

                $stmt= $pdo->prepare("Update maintenance set is_active= :cb , maintenance_message= :mm , maintained_by= :uid");
                $stmt->execute(array(':cb'=> $_POST['maintainActive'],
                ':mm'=> $_POST['maintainMessage'],
                ':uid'=> $_SESSION['user_id']));

                $_SESSION["success"] = "Maintenance Mode Updated";
            
        } else {
            if($_POST['maintainActive']){

                $stmt= $pdo->prepare("insert into maintenance (is_active, maintenance_message, maintained_by) values (:cb,:mm,:uid)");
                    $stmt->execute(array(':cb'=> $_POST['maintainActive'],
                    ':mm'=> $_POST['maintainMessage'],
                    ':uid'=> $_SESSION['user_id']));

                    $_SESSION["success"] = "Maintenance Mode Updated";
            } else{
                $_SESSION["error"] = "Select the checkbox to activate maintenance mode.";
            }

        }

?>